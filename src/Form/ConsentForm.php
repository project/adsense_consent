<?php

namespace Drupal\adsense_consent\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Form where end-users can consent to receive ads or not.
 */
class ConsentForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return '_adsense_consent_personalise_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['adsense_consent.personalise_form'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Default settings.
    $config = $this->config('adsense_consent.settings');
    $form = [
      '#form_id' => 'adsense_consent_consent_form',
      'consent_checkbox' => [
        '#type' => 'checkbox',
        '#title' => Html::escape($config->get('page_consent_label')),
        '#id' => 'adsense_consent_consent_chk',
      ],
      '#attached' => [
        'library' => [
          'adsense_consent/consent_form',
        ],
      ],
    ];
    return $form;
  }

  /**
   * We do nothing on submit, because everything is done with Javascript.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
