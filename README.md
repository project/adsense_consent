## INTRODUCTION

 - This module lets you offer visitors to your website a choice as to whether they
   will receive personalised ads from Google and third party ad providers.

## DISCLAIMER

 - This module comes without any warranty of any kind, including any
   warranty (implied or otherwise) as to its fitness for a particular purpose.
   In particular, whilst this module may help your website comply to privacy
   legislation in various jurisdictions, you and you alone are responsible for
   ensuring that your website complies with all necessary legislation.

 - Google and AdSense are trademarks of Google LLC.

## REQUIREMENTS

 - This module has the option to suppress all ads until a visitor has given
   explicit consent. That consent can be given by means of an optional checkbox
   on the "ad options" page. Alternatively this module can work with the EU
   Cookie Compliance module and use its cookie consent banner as the way to
   obtain consent for AdSense.

## INSTALLATION

 - After enabling the module, you need to visit
   `admin/config/services/adsense-consent`.
   Enter your Google AdSense Publisher ID. This module does nothing until you
   have done this.

## CONFIGURATION

The settings page has two remaining sections.

### 1. Global Settings

 - This allows you to enable Page Level Ads. These include "auto ads", such as
   vignettes, interstitial ads, anchor ads, and other kinds of ads that Google
   may introduce. Exactly which ads display on your site is then controlled from
   within your AdSense account settings.

### 2. Consent Page Settings

 - The module creates a page, accessible at the URL /ad-options, where visitors
   can find out more about the ad networks you use. You can modify the URL
   using the Path module in the way you'd expect.

 - By default, all visitors are served ads. You can give require explicit
   consent by checking either of the boxes headed "Require explicit consent
   before showing any ads." If you do this, NO ADS WILL BE SHOWN until they
   give their consent using the method you choose. If you check both boxes,
   a visitor can give consent by either method.

 - You can control the third party networks Google will use on your sites by
   visiting Allow & block ads > Content > All my sites > EU user consent.

 - A list of all Google third party networks is available at this URL:
   [support.google.com/adsense/answer/9012903](https://support.google.com/adsense/answer/9012903)

 - The default behaviour is for personalised ads to be served unless a user
   opts out, which they can do on the page this module creates. You can change
   this to show non-personalised ads unless a visitor opts in, or always to
   show non-personalised ads.

 - The module also lets you configure the text that appears on the /ad-options
   page.

 - The `/ad-options` page will display a list of the ad networks you use. You
   enter this list in the settings page, in the form "ad network, url", where
   the URL is the URL of the ad network's privacy information page. You can
   download this as a CSV from AdSense, giving exactly the ad networks active
   in your account.

 - Once again: You are responsible for ensuring the wording and settings you
   choose comply with applicable legislation.

 - In addition to configuration on the main settings page, you can place custom
   blocks.

### 3. Custom Block Settings

 - As well, or instead, of Page Level Ads, this module will give you a Drupal
   block that you can place in any block region you wish. This would then contain
   a single AdSense ad unit at a time.

 - You can use the site-wide Block Layout page to place a new AdSense Unit block.
   You will only be able to do this once you have set your AdSense Publisher ID
   site-wide (see above). In addition, each block you place will show a specific
   ad unit, the slot ID for which needs to be set when the block is placed. This
   can be changed later in the block's configuration settings.

 - As with all Drupal blocks, you can control the URLs / user roles / content
   types on which the block (and, hence, the ads) will appear.

 - To find the AdSense Unit ID, go to "Ad Units" in your Google AdSense account,
   and choose "Get Code" to view the code to insert to display the ad unit you
   want. The Ad Unit ID is the numeric value labelled "data-ad-slot".
