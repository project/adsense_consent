<?php

namespace Drupal\adsense_consent;

/**
 * Utility class for static regular expressions to be defined once then reused.
 */
class Helper {

  /**
   * Build a regular expression we can reuse to check valid ad provider.
   */
  public static function providerRegex() {
    return "~^(\S[a-zA-Z0-9 \,\.\-\/\(\)\&;" .
    html_entity_decode("&ndash;") . html_entity_decode("&mdash;") .
    html_entity_decode("&trade;") . "]+?),\s?(https?\:\/\/[a-zA-Z0-9\/\-_\.#=\?]+)$~";
  }

  /**
   * Build a regular expression we can reuse to check valid publisher ID.
   *
   * @var \Drupal\adsense_consent\Helper\pubidRegex
   */

  public static $pubidRegex = "/^pub-[0-9]+$/";

}
