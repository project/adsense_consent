<?php

namespace Drupal\adsense_consent\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a block to show a specific ad unit.
 *
 * @Block(
 *   id = "adsense_block",
 *   admin_label = @Translation("AdSense Unit"),
 *   category = @Translation("AdSense"),
 * )
 */
class AdsenseBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $module_config = \Drupal::config('adsense_consent.settings');
    $config = $this->getConfiguration();
    $pubid = $module_config->get('pubid');
    $slotid = $config['ad_unit_slot_id'];
    if ($pubid == "" || $slotid == "") {
      $markup = "&nbsp;";
    }
    else {
      $markup = '<ins class="adsbygoogle" style="display:block" data-ad-client="ca-' . $pubid . '" data-ad-slot="' . $slotid . '" data-ad-format="auto"></ins>';
    }
    return [
      '#markup' => $markup,
      '#attached' => [
        'library' => [
          'adsense_consent/push_ads',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $module_config = \Drupal::config('adsense_consent.settings');
    $config = $this->getConfiguration();
    $_adsense_consent_settings_url = Url::fromRoute('adsense_consent.admin_settings')->toString();
    $form['ad_unit_slot_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ad Unit ID'),
      '#default_value' => isset($config['ad_unit_slot_id']) ? $config['ad_unit_slot_id'] : '',
      '#description' => $this->t('The Slot ID of the Ad Unit to use in this block.'),
      '#required' => TRUE,
    ];
    $form['pubid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Publisher ID'),
      '#default_value' => $module_config->get('pubid'),
      '#description' => $this->t('The AdSense publisher ID is set on <a href=":url">the module settings page</a>.',
        [':url' => $_adsense_consent_settings_url]),
      '#required' => TRUE,
      '#disabled' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['ad_unit_slot_id'] = $values['ad_unit_slot_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $slot_regex = '/^\d{10}$/';
    if ($form_state->getValue('ad_unit_slot_id') != '' && !preg_match($slot_regex, $form_state->getValue('ad_unit_slot_id'))) {
      $form_state->setErrorByName('ad_unit_slot_id', $this->t('A valid AdSense Slot ID is exactly 10 digits (0-9)'));
    }
    if ($form_state->getValue('pubid') == '') {
      $form_state->setErrorByName('pubid', $this->t('You cannot place AdSense blocks until the AdSense Publisher ID has been set.'));
    }
  }

}
