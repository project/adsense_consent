<?php

namespace Drupal\adsense_consent\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Drupal\adsense_consent\Helper;

/**
 * Admin settings form for AdSense Consent module.
 */
class AdsenseConsentAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return '_adsense_consent_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('adsense_consent.settings');
    $config->set('pubid', $form_state->getValue('pubid'));
    $config->set('page_ads', $form_state->getValue('page_ads'));
    $config->set('wait_consent', $form_state->getValue('wait_consent'));
    $config->set('wait_eu_cookie_compliance', $form_state->getValue('wait_eu_cookie_compliance'));
    $config->set('personalise', $form_state->getValue('personalise'));
    $config->set('providers', $form_state->getValue('providers'));
    $config->set('page_text_personalise', $form_state->getValue('page_text_personalise'));
    $config->set('page_text_no_personalise', $form_state->getValue('page_text_no_personalise'));
    $config->set('page_text_footer_p', $form_state->getValue('page_text_footer_p'));
    $config->set('page_text_footer_no_p', $form_state->getValue('page_text_footer_no_p'));
    $config->set('page_text_ask_consent', $form_state->getValue('page_text_ask_consent'));
    $config->set('page_consent_label', $form_state->getValue('page_consent_label'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['adsense_consent.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    // Default settings.
    $config = $this->config('adsense_consent.settings');
    $form['AdSense Account'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('AdSense Account Settings'),
    ];
    $form['AdSense Account']['pubid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Publisher ID'),
      '#default_value' => $config->get('pubid'),
      '#description' => $this->t('The AdSense publisher ID, of the form pub-9999...'),
      '#required' => TRUE,
    ];
    $form['AdSense Global'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Global Settings'),
      '#description' => $this->t('Enable Page Ads for things like interstitial, anchor, vignette and auto ads. Exact ads are controlled within your AdSense account.'),
    ];
    $form['AdSense Global']['page_ads'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Page Ads'),
      '#default_value' => $config->get('page_ads'),
      '#description' => $this->t('Do you want to show Page Ads'),
    ];
    $_adsense_consent_page_url = Url::fromRoute('adsense_consent.options_page')->toString();
    $form['AdSense Consent Page'] = [
      '#type' => 'details',
      '#title' => $this->t('AdSense Consent Page Settings'),
      '#open' => TRUE,
      '#description' => $this->t(
         'Control <a href=":url">the page</a> where users can indicate their consent to personalised ads.',
        [':url' => $_adsense_consent_page_url]),
    ];
    $form['AdSense Consent Page']['wait'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Require explicit consent before showing any ads'),
      '#description' => $this->t('If the checkboxes below are all cleared, ads will be shown immediately. If any checkboxes are checked, visitors must give consent before ads will be shown, using any of the methods checked.'),
    ];
    $form['AdSense Consent Page']['wait']['wait_consent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require explicit consent from <a href=":url">ad options page</a>.',
        [':url' => $_adsense_consent_page_url]),
      '#default_value' => $config->get('wait_consent'),
    ];
    $form['AdSense Consent Page']['wait']['wait_eu_cookie_compliance'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require explicit consent from <a href=":url">EU Cookie Compliance module</a>.',
      [':url' => 'https://www.drupal.org/project/eu_cookie_compliance']),
      '#default_value' => $config->get('wait_eu_cookie_compliance'),
    ];
    if (!\Drupal::moduleHandler()->moduleExists('eu_cookie_compliance')) {
      $form['AdSense Consent Page']['wait']['wait_eu_cookie_compliance']['#attributes']['disabled'] = 'disabled';
    }
    $form['AdSense Consent Page']['personalise'] = [
      '#type' => 'radios',
      '#title' => $this->t('Personalise Ads'),
      '#default_value' => $config->get('personalise'),
      '#options' => [
        0 => $this->t('Show non-personalised ads all the time'),
        1 => $this->t('Show personalised ads unless a visitor has opted out'),
        2 => $this->t('Show non-personalised ads unless a visitor opts in'),
      ],
    ];
    $form['AdSense Consent Page']['providers'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Technology Providers'),
      '#default_value' => $config->get('providers'),
      '#description' => $this->t('List of all the AdSense Technology Providers used. Value should be a list of comma separated pairs: provider,url'),
      '#required' => TRUE,
    ];
    $text1 = $config->get('page_text_personalise');
    if ($text1 == '') {
      $text1 = [
        'format' => filter_fallback_format(),
        'value' => '',
      ];
    }
    $form['AdSense Consent Page']['Personalised'] = [
      '#type' => 'details',
      '#title' => $this->t('Text to display on consent page when visitors can decide whether to see personalised ads'),
      '#open' => FALSE,
    ];
    $form['AdSense Consent Page']['Not Personalised'] = [
      '#type' => 'details',
      '#title' => $this->t('Text to display on consent page when personalised ads are always turned off'),
      '#open' => FALSE,
    ];
    $form['AdSense Consent Page']['Obtain Consent'] = [
      '#type' => 'details',
      '#title' => $this->t('Text to display on consent page when consent is needed for ads'),
      '#open' => FALSE,
    ];
    $form['AdSense Consent Page']['Personalised']['page_text_personalise'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text at the top of the consent page'),
      '#default_value' => $text1['value'],
      '#format' => $text1['format'],
      '#required' => TRUE,
    ];
    $text2 = $config->get('page_text_no_personalise');
    if ($text2 == '') {
      $text2 = [
        'format' => filter_fallback_format(),
        'value' => '',
      ];
    }
    $form['AdSense Consent Page']['Not Personalised']['page_text_no_personalise'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text at the top of the consent page'),
      '#default_value' => $text2['value'],
      '#format' => $text2['format'],
      '#required' => TRUE,
    ];
    $text3 = $config->get('page_text_footer_p');
    if ($text3 == '') {
      $text3 = [
        'format' => filter_fallback_format(),
        'value' => '',
      ];
    }
    $form['AdSense Consent Page']['Personalised']['page_text_footer_p'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text at the foot of the consent page'),
      '#default_value' => $text3['value'],
      '#format' => $text3['format'],
      '#required' => FALSE,
    ];
    $text4 = $config->get('page_text_footer_no_p');
    if ($text4 == '') {
      $text4 = [
        'format' => filter_fallback_format(),
        'value' => '',
      ];
    }
    $form['AdSense Consent Page']['Not Personalised']['page_text_footer_no_p'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text at the foot of the consent page'),
      '#default_value' => $text4['value'],
      '#format' => $text4['format'],
      '#required' => FALSE,
    ];
    $text5 = $config->get('page_text_ask_consent');
    if ($text5 == '') {
      $text5 = [
        'format' => filter_fallback_format(),
        'value' => '',
      ];
    }
    $form['AdSense Consent Page']['Obtain Consent']['page_text_ask_consent'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text to ask for consent if explicit consent is required and the ad options page is the place you obtain it'),
      '#default_value' => $text5['value'],
      '#format' => $text5['format'],
      '#required' => FALSE,
    ];
    $form['AdSense Consent Page']['Obtain Consent']['page_consent_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The label next to the "consent" checkbox'),
      '#default_value' => $config->get('page_consent_label'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Validate AdSense Consent settings form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $_adsense_consent_parse_reg = Helper::providerRegex();
    $_adsense_consent_pubid_reg = Helper::$pubidRegex;
    if (!preg_match($_adsense_consent_pubid_reg, $form_state->getValue('pubid'))) {
      $form_state->setErrorByName('pubid', $this->t('A valid AdSense Publisher ID is case sensitive and formatted like %id.', ['%id' => 'pub-9999999999999']));
    }
    $providers = preg_split('/\r\n/', $form_state->getValue('providers'));
    $provider_errors = "";
    foreach ($providers as $provider) {
      if ($provider != "" && preg_match($_adsense_consent_parse_reg, $provider) == 0) {
        $provider_errors .= '<li>' . Html::escape($provider) . "</li>";
      }
    }
    if ($provider_errors != "") {
      $provider_errors = Markup::create('<ul>' . $provider_errors . '</ul>');
      $format_needed = Markup::create('{<em><code>provider, url</code></em>}');
      $form_state->setErrorByName('providers',
        new TranslatableMarkup('The following lines in the provider list were not of the required format @format:<br />@providers',
        [
          '@format' => $format_needed,
          '@providers' => $provider_errors,
        ]
        ));
    }

    if (!FilterFormat::load($form_state->getValue('page_text_personalise')['format'])->access('use')) {
      $form_state->setErrorByName('page_text_personalise', $this->t('Invalid text format chosen'));
    }
    if (!FilterFormat::load($form_state->getValue('page_text_no_personalise')['format'])->access('use')) {
      $form_state->setErrorByName('page_text_no_personalise', $this->t('Invalid text format chosen'));
    }
    if (!FilterFormat::load($form_state->getValue('page_text_footer_p')['format'])->access('use')) {
      $form_state->setErrorByName('page_text_footer_p', $this->t('Invalid text format chosen'));
    }
    if (!FilterFormat::load($form_state->getValue('page_text_footer_no_p')['format'])->access('use')) {
      $form_state->setErrorByName('page_text_footer_no_p', $this->t('Invalid text format chosen'));
    }
    if (!FilterFormat::load($form_state->getValue('page_text_ask_consent')['format'])->access('use')) {
      $form_state->setErrorByName('page_text_ask_consent', $this->t('Invalid text format chosen'));
    }
  }

}
