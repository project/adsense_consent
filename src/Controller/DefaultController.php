<?php

namespace Drupal\adsense_consent\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\adsense_consent\Helper;

/**
 * Default controller for the adsense_consent module.
 */
class DefaultController extends ControllerBase {

  /**
   * Define the main options page for the adsense_consent module.
   */
  public function adsenseConsentOptionsPage() {
    $_adsense_consent_parse_reg = Helper::providerRegex();
    $config = $this->config('adsense_consent.settings');
    $page = [];
    if ($config->get('wait_consent') == 1) {
      $consent_variable = $config->get('page_text_ask_consent');
      $format = isset($consent_variable["format"]) ? $consent_variable["format"] : -1;
      $value = isset($consent_variable["value"]) ? $consent_variable["value"] : "";
      $explanation_text = '<h3>' . $this->t("Advertising Consent") . "</h3>";
      $explanation_text .= ($format == -1 || $value == "") ? "" : check_markup($value, $format);
      $page['consent_explanation'] = [
        '#markup' => $explanation_text,
      ];
      $page['consent_form'] = $this->formBuilder()->getForm('Drupal\adsense_consent\Form\ConsentForm');
    }
    $personalise_setting = $config->get('personalise');
    $personalise_markup = ($personalise_setting == 0) ?
      $config->get('page_text_no_personalise') :
      $config->get('page_text_personalise');
    $format = isset($personalise_markup["format"]) ? $personalise_markup["format"] : -1;
    $value = isset($personalise_markup["value"]) ? $personalise_markup["value"] : "";
    $output1 = "<h3>" . $this->t("Ad Personalisation Explained") . "</h3>" .
      (($format == -1 || $value == "") ?
        $this->t("Publishers used are:") :
      check_markup($value, $format));
    $page['personalise_explanation'] = [
      '#markup' => $output1,
    ];

    $provider_output = '<h3>' . $this->t('Third Party Ad Networks') . '</h3>' . '<div class="ad-provider-container"><ul class="ad-providers">';
    $providers_raw = $config->get('providers');
    $providers = preg_split('/\r\n/', $providers_raw);
    foreach ($providers as $provider) {
      $matches = [];
      if ($provider != "" && preg_match($_adsense_consent_parse_reg, $provider, $matches) == 1) {
        $provider_output .= '<li class="ad-provider"><a href="' . $matches[2] . '">' . $matches[1] . '</a></li>';
      }
    }
    $provider_output .= "</ul></div>";
    $page['provider_list'] = [
      '#markup' => $provider_output,
      '#attached' => [
        'library' => [
          'adsense_consent/provider_list',
        ],
      ],
    ];

    $footer_text = ($personalise_setting == 0) ?
      $config->get('page_text_footer_no_p') :
      $config->get('page_text_footer_p');
    $format = isset($footer_text["format"]) ? $footer_text["format"] : -1;
    $value = isset($footer_text["value"]) ? $footer_text["value"] : "";
    $footer_markup = ($format == -1 || $value == "") ? "" :
      check_markup($value, $format);
    $page['footer'] = [
      '#markup' => $footer_markup,
    ];
    if ($personalise_setting != 0) {
      $page['personalise_form_heading'] = [
        '#markup' => '<h3>' . $this->t('Show Personalised Ads?') . '</h3>',
      ];
      $page['personalise_form'] = $this->formBuilder()->getForm('Drupal\adsense_consent\Form\PersonaliseForm');
    }
    return $page;
  }

}
