<?php

namespace Drupal\adsense_consent\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form where end-users can check if they want personalised ads or not.
 */
class PersonaliseForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return '_adsense_consent_personalise_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['adsense_consent.personalise_form'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Default settings.
    $config = $this->config('adsense_consent.settings');
    $personalise_setting = $config->get('personalise');
    $form = [
      'personalise_checkbox' => [
        '#type' => 'radios',
        '#id' => 'adsense_consent_personalise',
        '#default_value' => $personalise_setting,
        '#options' => [
          1 => $this->t("Yes"),
          2 => $this->t("No"),
        ],
        1 => [
          '#id' => 'adsense_consent_personalise_yes',
        ],
        2 => [
          '#id' => 'adsense_consent_personalise_no',
        ],
      ],
      '#attached' => [
        'library' => [
          'adsense_consent/personalise_form',
        ],
      ],
    ];
    return $form;
  }

  /**
   * We do nothing on submit, because everything is done with Javascript.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
