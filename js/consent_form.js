/**
 * @file
 * Javascript utility for the consent form.
 */

var chkconsent = document.getElementById("adsense_consent_consent_chk");
if(document.cookie.indexOf("ad_consent=1") > -1) {
  chkconsent.checked = true;
}
chkconsent.addEventListener("click",function () {
    document.cookie = "ad_consent=" + (chkconsent.checked ? 1 : 0);
});
