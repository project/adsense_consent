switch (drupalSettings.adsenseConsent.applyPersonalisationPrefs.personalise) {
    // Always show non-personalised ads.
  case 0:
    (adsbygoogle = window.adsbygoogle || []).requestNonPersonalizedAds = 1;
    break;

    // Show personalised ads unless a visitor has opted out.
  case 1:
    if(document.cookie.indexOf("personalise_ads=no") > -1) {
      (adsbygoogle = window.adsbygoogle || []).requestNonPersonalizedAds = 1;
    }
    break;

    // Show non-personalised ads unless a visitor opts in.
  case 2:
    if(document.cookie.indexOf("personalise_ads=yes") == -1) {
      (adsbygoogle = window.adsbygoogle || []).requestNonPersonalizedAds = 1;
    }
    break;
}
