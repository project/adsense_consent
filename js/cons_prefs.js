var adConsentEU = drupalSettings.adsenseConsent.applyConsentPrefs.eu;
var adConsentAC = drupalSettings.adsenseConsent.applyConsentPrefs.ac;
var adConsentEuCookie = drupalSettings.adsenseConsent.applyConsentPrefs.eu_cookie;
var adConsentAcCookie = drupalSettings.adsenseConsent.applyConsentPrefs.ac_cookie;
if (!adConsentEU && !adConsentAC) {
  var adsenseConsent = true;
}
if (adConsentEU && !adConsentAC) {
  var adsenseConsent = document.cookie.indexOf(adConsentEuCookie + "=1") > 01 || document.cookie.indexOf(adConsentEuCookie + "=2") > -1;
}
if (!adConsentEU && adConsentAC) {
  var adsenseConsent = document.cookie.indexOf(adConsentAcCookie + "=1") > 01;
}
if (adConsentEU && adConsentAC) {
  var adsenseConsent = document.cookie.indexOf(adConsentEuCookie + "=1") > -1 || document.cookie.indexOf(adConsentEuCookie + "=2") > -1 || document.cookie.indexOf(adConsentAcCookie + "=1") > -1;
}
