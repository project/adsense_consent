(function ($, Drupal) {
  Drupal.behaviors.adsense = {
    attach: function (context, settings) {
      $('#block-adsenseunit', context).once('adsenseunit').each(function () {
        if(adsenseConsent) {
          (adsbygoogle = window.adsbygoogle || []).push({});
        }
      });
    }
  };
})(jQuery, Drupal);
