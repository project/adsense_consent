/**
 * @file
 * Javascript utility for the personalise choice form.
 */

var personalise_yes = document.getElementById("adsense_consent_personalise_yes");
var personalise_no = document.getElementById("adsense_consent_personalise_no");
if(document.cookie.indexOf("personalise_ads=no") > -1) {
    personalise_yes.checked = false;
    personalise_no.checked = true;
}
if(document.cookie.indexOf("personalise_ads=yes") > -1) {
    personalise_no.checked = false;
    personalise_yes.checked = true;
}
personalise_no.addEventListener("click",function () {
    document.cookie = "personalise_ads=no";
});
personalise_yes.addEventListener("click",function () {
    document.cookie = "personalise_ads=yes";
});
